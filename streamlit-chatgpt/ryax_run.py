#!/usr/bin/env python3
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import asyncio
import os
from pathlib import Path
import time

async def run(service, input_values: dict) -> None:
    print("Starting streamlit server...")
    os.environ["OPENAI_API_KEY"] = input_values["openai_key"]

    for k, v in os.environ.items():
        print(f'{k}={v}')

    config = []
    config.extend([
        "--global.developmentMode", "false",
        "--server.enableCORS", "false",
        "--server.port", input_values['ryax_endpoint_port'] if 'ryax_endpoint_port' in input_values else 8080,
        "--server.baseUrlPath", input_values['ryax_endpoint_prefix'] if 'ryax_endpoint_prefix' in input_values else '/streamlit',
        "--server.enableXsrfProtection", "false",
        "--server.headless", "true",
    ])
    print("Using configuration...")
    cmd = "streamlit run "
    for opt in config:
        cmd += " " + str(opt)
        print("   "+str(opt))
    cmd += " " + str(Path(__file__).parent.resolve() / Path("chatbot.py"))
    print(f"Final cmd => '{cmd}'")
    proc = await asyncio.subprocess.create_subprocess_shell(cmd)
    await proc.wait()
    while True:
        print("Process finished on the waiting loop...")
        time.sleep(1)

if __name__ == "__main__":
    input_json = {
        "ryax_endpoint_prefix": "/streamlit",
        "ryax_endpoint_port": 8080,
        "openai_key": "secret",
    }

    # Populate with secrets for testing
    import json

    with open("./secrets.txt") as f:
        secrets = json.load(f)
        for key in secrets:
            if key in input_json:
                input_json[key] = secrets[key]

    from unittest.mock import AsyncMock

    asyncio.run(run(AsyncMock(), input_json))
